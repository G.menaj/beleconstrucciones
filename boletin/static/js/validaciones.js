/*linkear*/
$(function () {
    $("#mi-formulario").validate({
        rules: {
            nombre: {
                required: true
            },
            apellido: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            password: "required",
            password2: {
                required: true,
                equalTo: "#password"
            }
        },
        messages: {
            nombre: {
                required: 'Ingrese su nombre'
            },
            apellido: {
                required: 'Ingrese su apellido'
            },
            email: {
                required: 'Ingresa tu correo electrónico',
                email: 'Formato de correo no válido'
            },
            password: {
                required: 'Ingresa una contraseña',
                minlength: 'Largo insuficiente'
            },
            password2: {
                required: 'Rengresa la contraseña',
                equalTo: 'Las contraseñas ingresadas no coinciden',
                minlength: 'Largo insuficiente'

            }
        }
    });
});
function soloLetras(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
    especiales = "8-37-39-46";

    tecla_especial = false
    for (var i in especiales) {
        if (key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial) {
        return false;
    }
}
$(function () {
    $("#formulario").validate({
        rules: {
            nombre: {
                required: true
            },
            correo: {
                required: true,
                email: true
            },
            telefono: {
                required: true

            },

            consulta: {
                required: true
                
            }
        },
            messages:{
                nombre: {
                    required: 'Ingrese su nombre'
                },
                correo: {
                    required: 'Ingresa tu correo electrónico',
                    email: 'Formato de correo no válido'
                },
                telefono:{
                    required: 'Ingrese su numero de telefono',
                    minlength: 'Largo insuficiente'
            },
            consulta:{
                required: 'llenar campos'
             
        }
        }
        });
        });
        
        $('button').click(function(){
            $('button').toggleClass('active');
            $('.title').toggleClass('active');
            $('nav').toggleClass('active');
          });






