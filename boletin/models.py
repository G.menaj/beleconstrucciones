
from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse

# Create your models here.
class Perfil_Usuario(models.Model):
    User = models.OneToOneField(User, on_delete = models.CASCADE, primary_key = True)
    nombre = models.CharField(max_length=200)
    edad = models.IntegerField(default=0)
    #genero = models.CharField(max_length=20)
    MASCULINO = 'M'
    FEMENINO = 'F'
    GENERO_OPCIONES = (
        (MASCULINO, 'Masculino'),
        (FEMENINO, 'Femenino'),
    )
    genero = models.CharField(
        max_length=20,
        choices=GENERO_OPCIONES,
        default=MASCULINO,
    )

    def __str__(self):
        return self.nombre
    
    def create(self, user):
        self.User = user

        try:
            self.save()
            return True
        except:
            return False

    def modificar(self):
        try:
            self.save()
            return True
        except:
            return False


class Servicio(models.Model):
    CONSTRUCCION_GENERAL = 'CG'
    FIERROS_METALICOS = 'FM'
    SERVICIO_OPCIONES = (
        (CONSTRUCCION_GENERAL, 'Construccion General'),
        (FIERROS_METALICOS, 'Fierros Metalicos'),
    )
    nombre_servicio = models.CharField(max_length=100, default="Servicio para la empresa RUT ")
    rut_empresa = models.CharField(max_length=10)
    nombre_empresa = models.CharField(max_length=200)
    cantidad_trabajadores = models.IntegerField(default=0)
    servicio_requerido = models.CharField(
        max_length=2,
        choices=SERVICIO_OPCIONES,
        default=CONSTRUCCION_GENERAL,
    )
    inicio_contrato = models.DateField (auto_now=False, auto_now_add=False)
    termino_contrato = models.DateField (auto_now=False, auto_now_add=False)
  
    usuario_solicitante = models.ForeignKey(User, on_delete=models.CASCADE) ##Si el usuario que lo crea se borra, el servicio tambien

    def __str__(self):
        return self.nombre_servicio
    
    def modificarServicio(self):
        try:
            self.save()
            return True
        except:
            return False

    def crearServicio(self):
        try:
            self.save()
            return True
        except:
            return False

    
    def buscar_all(self,user):
        servicio = Servicio.objects.filter(usuario_solicitante=user)
        return servicio

    def buscar(self,id):
        servicio = Servicio.objects.get(id=id)

        self.id = servicio.id
        self.diaRnombre_servicioecordatorio = servicio.nombre_servicio
        self.rut_empresa = servicio.rut_empresa
        self.nombre_empresa = servicio.nombre_empresa
        self.cantidad_trabajadores = servicio.cantidad_trabajadores
        self.servicio_requerido = servicio.servicio_requerido
        self.inicio_contrato = servicio.inicio_contrato
        self.termino_contrato = servicio.termino_contrato
        self.usuario_solicitante = servicio.usuario_solicitante
        return servicio

    def eliminar(self,id):
        try:
            self.buscar(id)
            self.delete()
            return True
        except:
            return False


   

    



    ##Cuando se llame al servicio, se imprimira mostrando el rut de la empresa:

    # al crearse, obtiene una url del detalle del post creado
    


