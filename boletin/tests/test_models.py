import pytest
from mixer.backend.django import mixer
pytestmark = pytest.mark.django_db


class TestPerfilUsuario:

    def test_model(self):
        obj = mixer.blend('boletin.Perfil_Usuario')
        assert obj.pk == 1, 'Mensaje de error'

    def test_str(self):
        nombre = "Nombre de Prueba"
        obj = mixer.blend('boletin.Perfil_Usuario', nombre=nombre)

        assert str(obj) == nombre

    def testcreate(self):
        obj = mixer.blend('boletin.Perfil_Usuario')
        assert obj.create(obj.User) == True

    def test_modificar(self):
        nombre = 'Nombre modificado'
        obj = mixer.blend('boletin.Perfil_Usuario')
        obj.nombre = nombre
        assert obj.modificar() == True
    

class TestServicio:
    def test_model(self):
        obj = mixer.blend('boletin.Servicio')
        assert obj.pk == 1, 'Mensaje de error'


    def test_str(self):
        nombre_servicio = "Nombre Servicio de Prueba"
        obj = mixer.blend('boletin.Servicio', nombre_servicio=nombre_servicio)

        assert str(obj) == nombre_servicio
    

    def test_crearServicio(self):
        servicio = mixer.blend('boletin.Servicio')
        assert servicio.crearServicio() == True

    def test_buscar(self):
        servicio = mixer.blend('boletin.Servicio')
        servicio.crearServicio()
        servicio = mixer.blend('boletin.Servicio')
        servicio.buscar(servicio.id)
        assert servicio == servicio

    def test_modificar(self):
        update = 'Tittle Update'
        servicio = mixer.blend('boletin.Servicio')
        servicio.titulo = update
        assert servicio.modificarServicio() == True


    def test_eliminar(self):
        servicio = mixer.blend('boletin.Servicio')
        assert servicio.eliminar(servicio.id)
    

 