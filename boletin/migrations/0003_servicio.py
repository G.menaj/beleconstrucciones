# Generated by Django 2.2.6 on 2019-10-22 22:42

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('boletin', '0002_auto_20191019_1929'),
    ]

    operations = [
        migrations.CreateModel(
            name='Servicio',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('rut_empresa', models.CharField(max_length=10)),
                ('nombre_empresa', models.CharField(max_length=200)),
                ('cantidad_trabajadores', models.IntegerField(default=0)),
                ('servicio_requerido', models.CharField(choices=[('CG', 'Construccion General'), ('FM', 'Fierros Metalicos')], default='CG', max_length=2)),
                ('inicio_contrato', models.DateField()),
                ('termino_contrato', models.DateField()),
                ('usuario_solicitante', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
