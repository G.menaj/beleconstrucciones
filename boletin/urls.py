from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('consultar/', views.consultar, name='consultar'),
    path('nosotros/', views.nosotros, name='nosotros'),
    path('registrar/', views.registrar, name='registro'),
    path('perfil/', views.perfil, name = 'perfil'),

    #funciones para logear, cerrar, diccionario de datos 
    path('crear/', views.crear, name = 'crear'), #registrar usuario
    path('logear/', views.logear, name = 'logear'), #logear con usuario
    path('cerrar/', views.cerrar, name = 'cerrar'), #cerrar sesión
    path('diccionario/', views.diccionario, name ='diccionario'), #diccionario de datos
    path('modificar/', views.modificar, name ='modificar'), #modificar  datos de perfil
    path('cambiar/', views.cambiar, name ='cambiar'),
    path('registroServ/', views.registroServ, name="registroServ"),
    path('registrarServicio/', views.registrarServicio, name ='registrarServicio'),
    path('listarServicios/', views.listarServicios, name='listarServicios'),
    path('detalleServicio/<id>/', views.detalleServicio, name='detalleServicio'),
    path('eliminar/<id>/', views.eliminar, name='eliminar')


]

