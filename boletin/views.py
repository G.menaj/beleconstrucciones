
from django.contrib.auth.models import User
from .models import Perfil_Usuario, Servicio
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login
from django.shortcuts import render, redirect
from django.contrib.auth import logout as do_logout
from django.contrib.auth import authenticate

from django.contrib.auth import login as do_login
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail, BadHeaderError
from django.contrib import messages





# Create your views here.

def index(request):
    return render(request, 'boletin/index.html')


def consultar(request):
    return render(request, 'boletin/consulta.html')


def nosotros(request):
    return render(request, 'boletin/quienes_somos.html')


def registrar(request):
    return render(request, 'boletin/registro.html')


def perfil(request):
    return render(request, 'boletin/perfil.html')

def registroServ(request):
    return render(request, 'boletin/servicio_form.html')



def crear(request):

    if request.method == 'POST':
        user = User.objects.create_user(request.POST.get(
            'usuario'), request.POST.get('correo'), request.POST.get('pass1'))

        perfil = Perfil_Usuario()

        perfil.User = user
        perfil.nombre = request.POST.get('nombre')
        perfil.edad = request.POST.get('edad')
        perfil.genero = request.POST.get('genero')
        perfil.create(user)

    return (HttpResponseRedirect('/'))

#logear usuario
@csrf_exempt
def logear(request):
    if request.method == 'POST':
        usuario = request.POST.get('usuarioL')
        cont = request.POST.get('passL')
        user = authenticate(request, username=usuario, password=cont)
        if user is not None:
            login(request, user)
            return (HttpResponseRedirect('/listarServicios/'))
        else:
             return (HttpResponseRedirect('/'))
    else:
        return (HttpResponse('404 Not Found'))

#cerrar sesion
@login_required
def cerrar(request):
    do_logout(request)
    return(HttpResponseRedirect('/'))

#diccionario de datos
def diccionario(request):
    usuario =  request.user
    perfilUsuario = Perfil_Usuario.objects.get(User = usuario)
    datos_perfil = {'usuario': perfilUsuario.User,'nombre': perfilUsuario.nombre, 'genero' : perfilUsuario.genero , 'email': usuario.email , 'edad' : perfilUsuario.edad}
    return render (request, 'boletin/perfil.html', datos_perfil)
    
#actualizar datos de usuario
def modificar (request):
    if request.method == 'POST':
        usuario= request.user
        nomuser =Perfil_Usuario.objects.get(User = usuario )
        perfil = Perfil_Usuario.objects.get(pk = nomuser)
        perfil.nombre = request.POST.get('nombre')
        perfil.edad = request.POST.get('edad')
        perfil.genero = request.POST.get('genero')
        perfil.save()
        return HttpResponseRedirect ('/diccionario/')



#funcion cambiar contrseña de usuario
def cambiar(request):
    if request.method == 'POST':
        user = request.user
        contn = request.POST.get('pass2')
        user.set_password(contn)
        user.save()
        return HttpResponseRedirect ('/') 

#crud crear registro de servicio
def registrarServicio(request):
    user = request.user
    servicios= Servicio().buscar_all(user)
    idservicio = {
        'servicioS':servicios
    }
  
    if request.method == 'POST':
        
        servicios = Servicio()
        servicios.nombre_servicio = request.POST.get('nombre_servicio')
        servicios.rut_empresa = request.POST.get('rut_empresa')
        servicios.nombre_empresa = request.POST.get('nombre_empresa')
        servicios.cantidad_trabajadores = request.POST.get('cantidad_trabajadores')
        servicios.servicio_requerido = request.POST.get("servicio_requerido")
        servicios.inicio_contrato = request.POST.get('inicio_contrato')
        servicios.termino_contrato = request.POST.get('termino_contrato')
        servicios.usuario_solicitante = user

        try:
            servicios.crearServicio()
        except:
            return False

        return HttpResponseRedirect ('/listarServicios', idservicio) 


def listarServicios(request):
    user = request.user
    servicios = Servicio().buscar_all(user)
    idservicio = {
        'servicios':servicios
    }
    return render(request, 'boletin/listar_servicios.html', idservicio)


def detalleServicio(request, id):
    servicio = Servicio().buscar(id)
    idservicio = {
        'servicio':servicio
    }
    return render(request, 'boletin/modificar.html', idservicio)

def eliminar(request, id):
    servicio = Servicio().buscar(id)

    try:
        servicio.eliminar(id)
        mensaje = 'Eliminado Correctamente'
        messages.success(request,mensaje)
    except:
        mensaje = 'No se pudo Eliminar'
        messages.error(request,mensaje)

    return redirect('/listarServicios')




