# Generated by Django 2.2.6 on 2019-10-23 00:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('boletin', '0004_servicio_nombre_servicio'),
    ]

    operations = [
        migrations.AlterField(
            model_name='servicio',
            name='nombre_servicio',
            field=models.CharField(default='Servicio para la empresa RUT ', max_length=100),
        ),
    ]
